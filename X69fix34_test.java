package tmp;

import java.util.Arrays;
/**
 *  Write a one-sentence summary of your class here.
 *  Follow it with additional details about its purpose, what abstraction
 *  it represents, and how to use it.
 * 
 *  @author mdecker
 *  @version Oct 2, 2017
 */
public class Homework6
{

    /**
     * Place a description of your method here.
     * @param args
     */
    public static void main(String[] args)
    {
        if (Arrays.equals(fix34(new int[]{1, 3, 1, 4}), new int[] {1, 3, 4, 1})) { System.out.println("[1, 3, 4, 1] Success\n"); } else { System.out.println("[1, 3, 4, 1] FAIL\n"); }
        if (Arrays.equals(fix34(new int[]{1, 3, 1, 4, 4, 3, 1}), new int[] {1,3,4,1,1,3,4})) { System.out.println("[1, 3, 4, 1, 1, 3, 4] Success\n"); } else { System.out.println("[1, 3, 4, 1, 1, 3, 4] FAIL\n"); }
        if (Arrays.equals(fix34(new int[]{3,2,2,4}), new int[] {3,4,2,2})) { System.out.println("[3, 4, 2, 2] Success\n"); } else { System.out.println("[3, 4, 2, 2] FAIL\n"); }
        if (Arrays.equals(fix34(new int[]{3, 2, 3, 2, 4, 4}), new int[] {3, 4, 3, 4, 2, 2})) { System.out.println("[3, 4, 3, 4, 2, 2] Success\n"); } else { System.out.println("[3, 4, 3, 4, 2, 2] FAIL\n"); }
        if (Arrays.equals(fix34(new int[]{2, 3, 2, 3, 2, 4, 4}), new int[] {2, 3, 4, 3, 4, 2, 2})) { System.out.println("[2, 3, 4, 3, 4, 2, 2] Success\n"); } else { System.out.println("[2, 3, 4, 3, 4, 2, 2] FAIL\n"); }
        if (Arrays.equals(fix34(new int[]{3, 1, 4}), new int[] {3, 4, 1})) { System.out.println("[3, 4, 1] Success\n"); } else { System.out.println("[3, 4, 1] FAIL\n"); }
        if (Arrays.equals(fix34(new int[]{3, 1, 4, 3, 1, 4}), new int[] {3, 4, 1, 3, 4, 1})) { System.out.println("[3, 4, 1, 3, 4, 1] Success\n"); } else { System.out.println("[3, 4, 1, 3, 4, 1] FAIL\n"); }
        if (Arrays.equals(fix34(new int[]{3, 1, 1, 3, 4, 4}), new int[] {3, 4, 1, 3, 4, 1})) { System.out.println("[3, 4, 1, 3, 4, 1] Success\n"); } else { System.out.println("[3, 4, 1, 3, 4, 1] FAIL\n"); }
        
        /* if (Arrays.equals(fix34(new int[]{}), new int[] {})) { System.out.println("[] Success\n"); } else { System.out.println("[] FAIL\n"); } */

    }
    
    public static int[] fix34(int[] nums)
    {
        // do stuff
        return new int[]{0,0,0,0};
    } // fix34
}
